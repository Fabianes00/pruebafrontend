import { Component, OnInit } from '@angular/core';
import { Ej2Service } from './ej2.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-ej2',
  templateUrl: './ej2.component.html',
  styleUrls: ['./ej2.component.css']
})
export class Ej2Component implements OnInit {

  abc = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z'];
  datos: any = [];
  headers: any =  ['N°', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
    'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'Suma numericos contenidos'];
  cargando: boolean;
  msgError: string;
  subscription: Subscription;
  constructor(private ej2Service: Ej2Service) {
    this.cargando = false;
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (typeof this.subscription !== 'undefined') {
      this.subscription.unsubscribe();
    }
  }

  cargarDatos() {
    this.cargando = true;
    this.subscription = this.ej2Service.cargarData().subscribe( (ts: any) => {
      if (ts.success === true) {
        this.cargando = false;
        this.datos = this.llenarTabla(ts.data);
      } else {
        this.msgError = ts.error;
        this.cargando = false;
      }
    }, error => {
      this.msgError = error;
      this.cargando = false;
    });
  }

  /**
   * cuenta letras sin discriminar si son mayúsculas o minúsculas
   * @param data: array
   * return array con las apariciones de cada letra y suma de numeros
   */
  llenarTabla(data: any) {
    const output = [];
    JSON.parse(data).forEach((parrafo) => {
      const ocurrencias = {};
      this.abc.forEach((letter) => {
        ocurrencias[letter] = (parrafo.paragraph.match(new RegExp(letter, 'gi')) || []).length;
      });
      let sum = 0;
      (parrafo.paragraph.match(new RegExp('[1-9][0-9]*', 'gi')) || []).forEach((num) => {
        return sum += parseInt(num, 10);
      });
      ocurrencias['znumeros contenidos'] = sum;
      output.push(ocurrencias);
    });
    return output;
  }
}
