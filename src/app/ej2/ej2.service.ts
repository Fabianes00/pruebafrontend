import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class Ej2Service {

  constructor(private http: HttpClient) { }

  cargarData() {
    return this.http.get( environment.url + 'dict.php');
  }
}
