import { Component, OnInit } from '@angular/core';
import { Ej1Service } from './ej1.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-ej1',
  templateUrl: './ej1.component.html',
  styleUrls: ['./ej1.component.scss']
})
export class Ej1Component implements OnInit {

  datos: any = [];
  cargando: boolean;
  datosOrdenados: any = [];
  msgError: string;
  subscription: Subscription;
  constructor(private ej1Service: Ej1Service) {
    this.cargando = false;
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (typeof this.subscription !== 'undefined') {
      this.subscription.unsubscribe();
    }
  }

  cargarDatos() {
    this.cargando = true;
    this.subscription = this.ej1Service.cargarDatos().subscribe( (ts: any) => {
      if (ts.success === true) {
        this.datos = this.llenarTabla(ts.data);
        this.datosOrdenados = this.bubbleSort(ts.data);
        this.cargando = false;
      } else {
        this.msgError = ts.error;
        this.cargando = false;
      }
    }, error => {
      this.msgError = error;
      this.cargando = false;
    });
  }

  /**
   * ordena datos
   * @param datos: array con data
   * return array con data ordenada en objetos
   */
  llenarTabla(datos: number[]) {
    const ocurrencias  = {};
    const output: any = [];
    datos.forEach((num) => {
      ocurrencias[num] = (ocurrencias[num] || 0) + 1;
      const fila = {
        numero : num,
        ocurrencias : ocurrencias[num],
        first: datos.indexOf(num),
        last: datos.lastIndexOf(num)
      };
      const i = output.findIndex(x => x.numero === num);
      if (i === -1) {
        output.push(fila);
      } else {
        output.splice(i, 1, fila);
      }
    });
    return output;
  }

  /**
   * Ordenamiento por algoritmo bubblesort
   * @param array con numeros
   * return array ordenado
   */
  bubbleSort(array: number[]) {
    array.forEach(() => {
      array.slice(0, array.length - 1).forEach((j, index) => {
        if (array[index] > array[index + 1]) {
          const swap = array[index];
          array[index] = array[index + 1];
          array[index + 1] = swap;
        }
      });
    });
    return array;
  }
}
