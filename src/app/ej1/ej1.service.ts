import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class Ej1Service {

  constructor(private http: HttpClient) { }

  cargarDatos() {
    return this.http.get( environment.url + 'array.php');
  }
}
