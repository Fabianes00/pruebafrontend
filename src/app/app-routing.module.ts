import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Ej1Component } from './ej1/ej1.component';
import { Ej2Component } from './ej2/ej2.component';

const routes: Routes = [
  {path: 'ej1', component: Ej1Component},
  {path: 'ej2', component: Ej2Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
